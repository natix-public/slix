package io.natix.slix.requester.helper;

import io.natix.slix.did.helper.DIDHelper;
import io.natix.slix.did.constant.EncryptionKind;
import io.natix.slix.did.constant.UserType;
import io.natix.slix.did.payload.DIDDocument;
import io.natix.slix.did.payload.DIDDocumentRequest;
import io.natix.slix.did.payload.PublicKey;
import io.natix.slix.core.payload.ResponseBody;
import io.natix.slix.core.util.CryptoUtil;

public interface RequesterHelper extends DIDHelper {

    default ResponseBody<DIDDocument> registerToDID(String name, String ecdhPublicKey, String eddsaPublicKey) {
        PublicKey[] publicKeys = new PublicKey[]{
                new PublicKey(CryptoUtil.sha256(ecdhPublicKey), ecdhPublicKey, EncryptionKind.ECDH.name().toLowerCase()),
                new PublicKey(CryptoUtil.sha256(eddsaPublicKey), eddsaPublicKey, EncryptionKind.EDDSA.name().toLowerCase())
        };
        DIDDocumentRequest requestBody = new DIDDocumentRequest(name, publicKeys, null);
        return add(requestBody, UserType.REQUESTER);
    }

}
