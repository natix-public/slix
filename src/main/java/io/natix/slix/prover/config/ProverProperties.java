package io.natix.slix.prover.config;

public class ProverProperties {
    private int publishPort = 7200;

    private int replyPort = 7201;

    public int getPublishPort() {
        return this.publishPort;
    }

    public String getPublishURL() {
        return "tcp://*:" + getPublishPort();
    }

    public void setPublishPort(int publishPort) {
        this.publishPort = publishPort;
    }

    public int getReplyPort() {
        return this.replyPort;
    }

    public void setReplyPort(int replyPort) {
        this.replyPort = replyPort;
    }

    public ProverProperties() {
    }

    public ProverProperties(int publishPort, int replyPort) {
        this.publishPort = publishPort;
        this.replyPort = replyPort;
    }

    public static ProverProperties of() {
        return new ProverProperties();
    }

    public static ProverProperties of(int publishPort, int replyPort) {
        return new ProverProperties(publishPort, replyPort);
    }
}