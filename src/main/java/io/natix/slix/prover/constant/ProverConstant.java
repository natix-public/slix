package io.natix.slix.prover.constant;

import io.natix.slix.core.util.EnvironmentUtil;
import io.natix.slix.prover.config.ProverProperties;
import org.zeromq.ZContext;
import org.zeromq.ZMQ;

public class ProverConstant {

    public static ZContext Z_CONTEXT;
    public static ZMQ.Socket PUBLISH_SOCKET;
    public static ZMQ.Socket REPLY_SOCKET;
    public static Thread PUBLISH_THREAD;

    static int getPublishPort() {
        return Integer.parseInt(EnvironmentUtil.getPropertyValue("slix.prover.publishPort", "7200"));
    }

    static int getReplyPort() {
        return Integer.parseInt(EnvironmentUtil.getPropertyValue("slix.prover.replyPort", "7201"));
    }

    public static ProverProperties getProperties() {
        return ProverProperties.of(getPublishPort(), getReplyPort());
    }
}
