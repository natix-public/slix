package io.natix.slix.prover.helper;

import io.natix.slix.core.constant.HttpStatus;
import io.natix.slix.core.constant.PacketType;
import io.natix.slix.core.context.AppContextProvider;
import io.natix.slix.core.event.EventHandler;
import io.natix.slix.core.exception.HttpException;
import io.natix.slix.core.helper.LogHelper;
import io.natix.slix.core.helper.SerializeHelper;
import io.natix.slix.core.payload.Event;
import io.natix.slix.core.payload.ResponseBody;
import io.natix.slix.core.payload.ResponseMessage;
import io.natix.slix.core.payload.proto.Message;
import io.natix.slix.core.util.CryptoUtil;
import io.natix.slix.core.util.JsonUtil;
import io.natix.slix.did.constant.EncryptionKind;
import io.natix.slix.did.constant.UserType;
import io.natix.slix.did.helper.DIDHelper;
import io.natix.slix.did.payload.DIDDocument;
import io.natix.slix.did.payload.DIDDocumentRequest;
import io.natix.slix.did.payload.PublicKey;
import io.natix.slix.did.payload.Service;
import io.natix.slix.prover.constant.ProverConstant;
import io.natix.slix.prover.model.ProverDetail;
import io.natix.slix.prover.service.ProverLoaderService;
import io.natix.slix.subject.metadata.Messages;
import org.apache.commons.lang3.ObjectUtils;
import org.apache.commons.lang3.StringUtils;
import org.reflections.Reflections;
import org.zeromq.SocketType;
import org.zeromq.ZContext;
import org.zeromq.ZMQ;

import java.util.Set;
import java.util.UUID;
import java.util.concurrent.Executors;

public interface ProverHelper extends DIDHelper, SerializeHelper {

    default ResponseBody<DIDDocument> registerToDID(String name, String ecdhPublicKey, String eddsaPublicKey, String ip) {
        String subscribe = "tcp://" + ip + ":" + ProverConstant.getProperties().getPublishPort();
        String reply = "tcp://" + ip + ":" + ProverConstant.getProperties().getReplyPort();
        PublicKey[] publicKeys = new PublicKey[]{
                new PublicKey(EncryptionKind.ECDH.name().toLowerCase(), ecdhPublicKey, CryptoUtil.sha256(ecdhPublicKey)),
                new PublicKey(EncryptionKind.EDDSA.name().toLowerCase(), eddsaPublicKey, CryptoUtil.sha256(eddsaPublicKey))
        };
        Service[] services = new Service[]{
                new Service(UUID.randomUUID().toString(), "subscribe", subscribe),
                new Service(UUID.randomUUID().toString(), "reply", reply)
        };
        DIDDocumentRequest requestBody = new DIDDocumentRequest(name, publicKeys, services);
        return add(requestBody, UserType.PROVER);
    }

    default void start() {
        stop();
        Executors.newFixedThreadPool(1).execute(this::listenOnReplyPort);
        Executors.newFixedThreadPool(1).execute(this::connectToPublishPort);
    }

    private void stop() {
        if (ProverConstant.PUBLISH_THREAD != null)
            ProverConstant.PUBLISH_THREAD.interrupt();
        if (ProverConstant.PUBLISH_SOCKET != null)
            ProverConstant.PUBLISH_SOCKET.close();
        if (ProverConstant.REPLY_SOCKET != null)
            ProverConstant.REPLY_SOCKET.close();
        if (ProverConstant.Z_CONTEXT != null && !ProverConstant.Z_CONTEXT.isClosed())
            ProverConstant.Z_CONTEXT.close();
    }

    private ZContext getZContext() {
        if (ProverConstant.Z_CONTEXT == null || ProverConstant.Z_CONTEXT.isClosed())
            ProverConstant.Z_CONTEXT = new ZContext();
        return ProverConstant.Z_CONTEXT;
    }

    private ZMQ.Socket connectToPublishPort() {
        return getPublishSocket();
    }

    private ZMQ.Socket getPublishSocket() {
        if (ProverConstant.PUBLISH_SOCKET == null) {
            ProverConstant.PUBLISH_SOCKET = getZContext().createSocket(SocketType.PUB);
            String uri = "tcp://*:" + ProverConstant.getProperties().getPublishPort();
            ProverConstant.PUBLISH_SOCKET.bind(uri);
            LogHelper.info(StringUtils.EMPTY, "Connected to: " + uri);
        }
        return ProverConstant.PUBLISH_SOCKET;
    }

    default void publish(String topic, String message) {
        getPublishSocket().sendMore(topic);
        getPublishSocket().send(message);
    }

    default void publish(byte[] topic, byte[] message) {
        getPublishSocket().sendMore(topic);
        getPublishSocket().send(message);
    }

    default void publish(String topic, byte[] message) {
        getPublishSocket().sendMore(topic);
        getPublishSocket().send(message);
    }

    private void listenOnReplyPort() {
        ProverConstant.PUBLISH_THREAD = Thread.currentThread();
        ProverConstant.REPLY_SOCKET = getZContext().createSocket(SocketType.REP);
        String uri = "tcp://*:" + ProverConstant.getProperties().getReplyPort();
        ProverConstant.REPLY_SOCKET.bind(uri);
        LogHelper.info(StringUtils.EMPTY, "Listening on: " + uri);
        while (!ProverConstant.PUBLISH_THREAD.isInterrupted()) {
            byte[] reply = ProverConstant.REPLY_SOCKET.recv(0);
            byte[] response = parseMessage(reply).orElse(null);
            ProverConstant.REPLY_SOCKET.send(response, 0);
        }
    }

    private ResponseBody<byte[]> parseMessage(byte[] reply) {
        ResponseMessage response = null;
        String did = null;
        String publicKey = null;
        String privateKey = null;
        PacketType packetType = null;
        try {
            ResponseBody<ProverDetail> proverBody = findCurrentProver();
            if (proverBody.hasError())
                return ResponseBody.error(HttpStatus.NOT_FOUND, Messages.PROVER_NOT_FOUND);
            privateKey = proverBody.get().getEcdh().getPrivateKey();
            Message.EncryptedPacket encryptedPacket = Message.EncryptedPacket.parseFrom(reply);
            did = encryptedPacket.getDid();
            packetType = PacketType.valueOf(encryptedPacket.getType());
            ResponseBody<DIDDocument> document = findById(encryptedPacket.getDid());
            if (document.hasError())
                return ResponseBody.error(HttpStatus.NOT_FOUND, Messages.SUBJECT_NOT_FOUND);
            ResponseBody<String> key = findPublicKey(document.get(), EncryptionKind.ECDH);
            if (key.hasError())
                return ResponseBody.error(HttpStatus.NOT_FOUND, Messages.SUBJECT_NOT_FOUND);
            DIDDocument subject = document.get();
            publicKey = key.get();
            Set<Class<? extends EventHandler>> listeners = getReflection().getSubTypesOf(EventHandler.class);
            if (listeners == null || listeners.isEmpty())
                return ResponseBody.error(HttpStatus.NOT_FOUND, Messages.LISTENER_NOT_FOUND);
            for (Class<? extends EventHandler> listener : listeners) {
                String packetId = CryptoUtil.sha256(encryptedPacket.getData().toString());
                //TODO must be calc validity
                boolean validity = true;
                EventHandler instance = listener.getDeclaredConstructor().newInstance();
                byte[] data = CryptoUtil.ecdhDecrypt(privateKey, publicKey, encryptedPacket.getData().toByteArray());
                response = ResponseMessage.of(instance.execute(new Event(subject, packetId, validity, packetType, data)));
            }
        } catch (Throwable e) {
            LogHelper.error("ParseMessageError", e.getMessage());
            response = ResponseMessage.of(HttpStatus.SERVICE_UNAVAILABLE.value(), e.getMessage());
            if (e instanceof HttpException) {
                HttpException httpException = ((HttpException) e);
                response = ResponseMessage.of(httpException.code(), httpException.getMessage());
            }
        }
        if (ObjectUtils.allNotNull(privateKey, publicKey, packetType)) {
            byte[] data = CryptoUtil.ecdhEncrypt(privateKey, publicKey, JsonUtil.toString(response));
            Message.EncryptedPacket packet = encryptPacket(packetType, did, data);
            return ResponseBody.ok(packet.toByteArray());
        }
        return ResponseBody.error(response.getStatusCode(), response.getMessage());
    }

    private Reflections getReflection() {
        return new Reflections(getBasePackageName());
    }

    private String getBasePackageName() {
        String[] packages = getClass().getPackageName().split("\\.");
        return (packages.length <= 2) ? packages[0] : packages[0] + '.' + packages[1];
    }

    private <Prover extends ProverDetail> ResponseBody<Prover> findCurrentProver() {
        try {
            return (ResponseBody<Prover>) getProverLoader().execute();
        } catch (Throwable e) {
            if (e instanceof HttpException) {
                HttpException httpException = ((HttpException) e);
                return ResponseBody.error(httpException.code(), httpException.getMessage());
            }
            return ResponseBody.error(HttpStatus.SERVICE_UNAVAILABLE, e.getMessage());
        }
    }

    private ProverLoaderService<? extends ProverDetail> getProverLoader() {
        return AppContextProvider.context.getBean(ProverLoaderService.class);
    }

}
