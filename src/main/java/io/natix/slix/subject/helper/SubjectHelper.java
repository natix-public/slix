package io.natix.slix.subject.helper;

import io.natix.slix.core.constant.HttpStatus;
import io.natix.slix.core.constant.PacketType;
import io.natix.slix.core.context.AppContextProvider;
import io.natix.slix.core.event.EventHandler;
import io.natix.slix.core.exception.HttpException;
import io.natix.slix.core.helper.LogHelper;
import io.natix.slix.core.helper.SerializeHelper;
import io.natix.slix.core.helper.ZMQHelper;
import io.natix.slix.core.payload.CustomMessage;
import io.natix.slix.core.payload.Event;
import io.natix.slix.core.payload.ResponseBody;
import io.natix.slix.core.payload.ResponseMessage;
import io.natix.slix.core.payload.proto.Message;
import io.natix.slix.core.util.CryptoUtil;
import io.natix.slix.core.util.JsonUtil;
import io.natix.slix.did.constant.EncryptionKind;
import io.natix.slix.did.constant.ServiceKind;
import io.natix.slix.did.constant.UserType;
import io.natix.slix.did.helper.DIDHelper;
import io.natix.slix.did.payload.DIDDocument;
import io.natix.slix.did.payload.DIDDocumentRequest;
import io.natix.slix.did.payload.PublicKey;
import io.natix.slix.prover.payload.Form;
import io.natix.slix.prover.payload.ReadRegistrationStatusRequest;
import io.natix.slix.prover.payload.ReadRegistrationStatusResponse;
import io.natix.slix.prover.payload.RegistrationRequest;
import io.natix.slix.subject.metadata.Messages;
import io.natix.slix.subject.model.SubjectDetail;
import io.natix.slix.subject.payload.ProverDID;
import io.natix.slix.subject.service.SubjectLoaderService;
import org.apache.commons.lang3.StringUtils;

import java.nio.charset.StandardCharsets;
import java.util.Objects;
import java.util.concurrent.atomic.AtomicReference;

public interface SubjectHelper extends DIDHelper, ZMQHelper, SerializeHelper {

    default ResponseBody<DIDDocument> registerToDID(String name, String ecdhPublicKey, String eddsaPublicKey) {
        PublicKey[] publicKeys = new PublicKey[]{
                new PublicKey(CryptoUtil.sha256(ecdhPublicKey), ecdhPublicKey, EncryptionKind.ECDH.name().toLowerCase()),
                new PublicKey(CryptoUtil.sha256(eddsaPublicKey), eddsaPublicKey, EncryptionKind.EDDSA.name().toLowerCase())
        };
        DIDDocumentRequest requestBody = new DIDDocumentRequest(name, publicKeys, null);
        return add(requestBody, UserType.SUBJECT);
    }

    default ResponseBody<ProverDID> getProverDID(String did) {
        ResponseBody<DIDDocument> document = findByIdAndType(did, UserType.PROVER);
        if (document.hasError())
            return ResponseBody.error(HttpStatus.NOT_FOUND, Messages.PROVER_NOT_FOUND);
        ResponseBody<String> replyUri = findServiceEndpoint(document.get(), ServiceKind.REPLY);
        ResponseBody<String> subscribeUri = findServiceEndpoint(document.get(), ServiceKind.SUBSCRIBE);
        ResponseBody<String> ecdhPublicKey = findPublicKey(document.get(), EncryptionKind.ECDH);
        ResponseBody<String> eddsaPublicKey = findPublicKey(document.get(), EncryptionKind.EDDSA);
        if (ResponseBody.hasAnyError(replyUri, subscribeUri, ecdhPublicKey, eddsaPublicKey))
            return ResponseBody.error(HttpStatus.NOT_FOUND, Messages.PROVER_NOT_FOUND);
        return ResponseBody.ok(ProverDID.of(document.get(), replyUri.get(), subscribeUri.get(), ecdhPublicKey.get(), eddsaPublicKey.get(), document.get().getStatus()));
    }

    default ResponseBody<Boolean> connectToProver(String did, EventHandler handler) {
        ResponseBody<ProverDID> proverBody = getProverDID(did);
        ResponseBody<SubjectDetail> subjectBody = findCurrentSubject();
        if (ResponseBody.hasAnyError(proverBody, subjectBody))
            return ResponseBody.error(proverBody.hasError() ? proverBody : subjectBody);
        ProverDID proverDID = getProverDID(did).get();
        SubjectDetail subject = subjectBody.get();
        if (StringUtils.isBlank(subject.getDid()) || findByIdAndType(subject.getDid(), UserType.SUBJECT).hasError())
            return ResponseBody.error(HttpStatus.NOT_FOUND, Messages.SUBJECT_MUST_BE_REGISTER_IN_DID);
        subscribe(proverDID.getSubscribeUri(), subject.getDid(), (topic, msg) -> {
            try {
                Message.EncryptedPacket encryptedPacket = decryptPacket(msg);
                DIDDocument doc = findByIdAndType(encryptedPacket.getDid(), UserType.PROVER).orElse(null);
                if (doc == null)
                    return;
                String packetId = CryptoUtil.sha256(encryptedPacket.getData().toString());
                PacketType packetType = PacketType.valueOf(encryptedPacket.getType());
                //TODO must be calc validity
                boolean validity = false;
                byte[] data = CryptoUtil.ecdhDecrypt(subject.getEcdh().getPrivateKey(), proverDID.getEcdhPublicKey(), encryptedPacket.getData().toByteArray());
                handler.execute(new Event(doc, packetId, validity, packetType, data));
            } catch (Throwable e) {
                LogHelper.error("OnReceivedMessage", e.getMessage());
            }
        });
        return ResponseBody.ok(true);
    }

    default <Subject extends SubjectDetail> ResponseBody<Subject> findCurrentSubject() {
        try {
            return (ResponseBody<Subject>) getSubjectLoader().execute();
        } catch (Throwable e) {
            if (e instanceof HttpException) {
                HttpException httpException = ((HttpException) e);
                return ResponseBody.error(httpException.code(), httpException.getMessage());
            }
            return ResponseBody.error(HttpStatus.SERVICE_UNAVAILABLE, e.getMessage());
        }
    }

    default ResponseBody<Form> getProverForm(String did) {
        try {
            ResponseBody<ProverDID> proverBody = getProverDID(did);
            ResponseBody<SubjectDetail> subjectBody = findCurrentSubject();
            if (ResponseBody.hasAnyError(proverBody, subjectBody))
                return ResponseBody.error(proverBody.hasError() ? proverBody : subjectBody);
            ProverDID prover = getProverDID(did).get();
            SubjectDetail subject = subjectBody.get();
            CustomMessage customMessage = new CustomMessage("GET_FORMS");
            byte[] encypted = CryptoUtil.ecdhEncrypt(subject.getEcdh().getPrivateKey(), prover.getEcdhPublicKey(), JsonUtil.toString(customMessage));
            Message.EncryptedPacket encryptedPacket = encryptPacket(PacketType.Custom, subject.getDid(), encypted);
            AtomicReference<byte[]> callback = new AtomicReference<>();
            send(prover.getReplyUri(), encryptedPacket.toByteArray(), callback::set);
            Message.EncryptedPacket callbackPacket = decryptPacket(callback.get());
            String strResponse = new String(Objects.requireNonNull(CryptoUtil.ecdhDecrypt(subject.getEcdh().getPrivateKey(), prover.getEcdhPublicKey(), callbackPacket.getData().toByteArray())), StandardCharsets.UTF_8);
            ResponseMessage message = JsonUtil.toObject(strResponse, ResponseMessage.class);
            if (message != null && message.getStatusCode() != null) {
                if (message.getStatusCode() == 200)
                    return ResponseBody.ok(JsonUtil.toObject(message.getMessage(), Form.class));
                else
                    return ResponseBody.error(message.getStatusCode(), message.getMessage());
            }
            return ResponseBody.error(HttpStatus.SERVICE_UNAVAILABLE, Messages.PROVER_SERVICE_IS_UNAVAILABLE);
        } catch (Exception e) {
            LogHelper.error("GetFormException", e.getMessage());
            return ResponseBody.error(HttpStatus.SERVICE_UNAVAILABLE, Messages.PROVER_SERVICE_IS_UNAVAILABLE);
        }
    }

    default ResponseBody<Boolean> registerToProver(String did, RegistrationRequest request) {
        try {
            ResponseBody<ProverDID> proverBody = getProverDID(did);
            ResponseBody<SubjectDetail> subjectBody = findCurrentSubject();
            if (ResponseBody.hasAnyError(proverBody, subjectBody))
                return ResponseBody.error(proverBody.hasError() ? proverBody : subjectBody);
            ProverDID prover = getProverDID(did).get();
            SubjectDetail subject = subjectBody.get();
            CustomMessage customMessage = new CustomMessage("REGISTER_SUBJECT", JsonUtil.toString(request));
            byte[] data = CryptoUtil.ecdhEncrypt(subject.getEcdh().getPrivateKey(), prover.getEcdhPublicKey(), JsonUtil.toString(customMessage));
            Message.EncryptedPacket encryptedPacket = encryptPacket(PacketType.Custom, subject.getDid(), data);
            AtomicReference<byte[]> callback = new AtomicReference<>();
            send(prover.getReplyUri(), encryptedPacket.toByteArray(), callback::set);
            Message.EncryptedPacket callbackPacket = decryptPacket(callback.get());
            String strResponse = new String(Objects.requireNonNull(CryptoUtil.ecdhDecrypt(subject.getEcdh().getPrivateKey(), prover.getEcdhPublicKey(), callbackPacket.getData().toByteArray())), StandardCharsets.UTF_8);
            ResponseMessage message = JsonUtil.toObject(strResponse, ResponseMessage.class);
            if (message != null && message.getStatusCode() != null) {
                if (message.getStatusCode() == 200) {
                    return ResponseBody.ok(true);
                } else
                    return ResponseBody.error(message.getStatusCode(), message.getMessage());
            }
            return ResponseBody.error(HttpStatus.SERVICE_UNAVAILABLE, Messages.PROVER_SERVICE_IS_UNAVAILABLE);
        } catch (Exception e) {
            LogHelper.error("GetFormException", e.getMessage());
            return ResponseBody.error(HttpStatus.SERVICE_UNAVAILABLE, Messages.PROVER_SERVICE_IS_UNAVAILABLE);
        }
    }

    default ResponseBody<ReadRegistrationStatusResponse> readSubjectStatus(String did, ReadRegistrationStatusRequest request) {
        try {
            ResponseBody<ProverDID> proverBody = getProverDID(did);
            ResponseBody<SubjectDetail> subjectBody = findCurrentSubject();
            if (ResponseBody.hasAnyError(proverBody, subjectBody))
                return ResponseBody.error(proverBody.hasError() ? proverBody : subjectBody);
            ProverDID prover = getProverDID(did).get();
            SubjectDetail subject = subjectBody.get();
            CustomMessage customMessage = new CustomMessage("GET_SUBJECT_STATUS", JsonUtil.toString(request));
            byte[] data = CryptoUtil.ecdhEncrypt(subject.getEcdh().getPrivateKey(), prover.getEcdhPublicKey(), JsonUtil.toString(customMessage));
            Message.EncryptedPacket encryptedPacket = encryptPacket(PacketType.Custom, subject.getDid(), data);
            AtomicReference<byte[]> callback = new AtomicReference<>();
            send(prover.getReplyUri(), encryptedPacket.toByteArray(), callback::set);
            Message.EncryptedPacket callbackPacket = decryptPacket(callback.get());
            String strResponse = new String(Objects.requireNonNull(CryptoUtil.ecdhDecrypt(subject.getEcdh().getPrivateKey(), prover.getEcdhPublicKey(), callbackPacket.getData().toByteArray())), StandardCharsets.UTF_8);
            ResponseMessage message = JsonUtil.toObject(strResponse, ResponseMessage.class);
            if (message != null && message.getStatusCode() != null) {
                if (message.getStatusCode() == 200)
                    return ResponseBody.ok(JsonUtil.toObject(message.getMessage(), ReadRegistrationStatusResponse.class));
                else
                    return ResponseBody.error(message.getStatusCode(), message.getMessage());
            }
            return ResponseBody.error(HttpStatus.SERVICE_UNAVAILABLE, Messages.PROVER_SERVICE_IS_UNAVAILABLE);
        } catch (Exception e) {
            LogHelper.error("GetFormException", e.getMessage());
            return ResponseBody.error(HttpStatus.SERVICE_UNAVAILABLE, Messages.PROVER_SERVICE_IS_UNAVAILABLE);
        }
    }

    private SubjectLoaderService<? extends SubjectDetail> getSubjectLoader() {
        return AppContextProvider.context.getBean(SubjectLoaderService.class);
    }

}
