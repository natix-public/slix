package io.natix.slix.subject.service;

import io.natix.slix.core.payload.ResponseBody;
import io.natix.slix.subject.model.SubjectDetail;

import java.util.Optional;

public interface SubjectLoaderService<T extends SubjectDetail> {

    ResponseBody<T> execute() throws Throwable;

}
