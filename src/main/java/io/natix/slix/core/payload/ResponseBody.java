package io.natix.slix.core.payload;

import io.natix.slix.core.exception.HttpException;
import io.natix.slix.core.message.MessageHandler;
import io.natix.slix.core.constant.HttpStatus;
import org.apache.commons.lang3.StringUtils;
import retrofit2.Response;

import java.util.Objects;
import java.util.Optional;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.function.Supplier;

public class ResponseBody<T> {
    private T value;

    private HttpStatus status;

    private String message;

    public T get() {
        return value;
    }

    public void setValue(T value) {
        this.value = value;
    }

    public HttpStatus getStatus() {
        return status;
    }

    public void setStatus(HttpStatus status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public boolean isSuccess() {
        return getStatus() != null && getStatus().equals(HttpStatus.OK);
    }

    public boolean hasError() {
        return !isSuccess();
    }

    public <U> ResponseBody<U> map(Function<? super T, ? extends U> mapper) {
        Objects.requireNonNull(mapper);
        if (!isSuccess()) {
            return ResponseBody.error(status, message);
        } else {
            return ResponseBody.ok(mapper.apply(value));
        }
    }

    public ResponseBody<T> or(Supplier<? extends ResponseBody<? extends T>> supplier) {
        Objects.requireNonNull(supplier);
        if (isSuccess()) {
            return this;
        } else {
            @SuppressWarnings("unchecked")
            ResponseBody<T> r = (ResponseBody<T>) supplier.get();
            return Objects.requireNonNull(r);
        }
    }

    public <X extends Throwable> T orElseThrow(Supplier<? extends X> supplier) throws X {
        if (value != null) {
            return value;
        } else {
            throw supplier.get();
        }
    }

    public T orElseThrow() {
        if (value == null) {
            throw new HttpException(message, HttpStatus.valueOf(status.value()));
        }
        return value;
    }

    public T orElse(T other) {
        return value != null ? value : other;
    }

    public ResponseBody<T> filter(Predicate<? super T> predicate) {
        Objects.requireNonNull(predicate);
        if (!isSuccess()) {
            return this;
        } else {
            return predicate.test(value) ? this : empty();
        }
    }

    public ResponseBody() {
    }

    public ResponseBody(HttpStatus status, T value, String message) {
        this.status = status;
        this.value = value;
        this.message = message;
    }

    public static <T> ResponseBody<T> ok(T data) {
        return of(HttpStatus.OK, data, StringUtils.EMPTY);
    }

    public static <T> ResponseBody<T> empty() {
        return error(HttpStatus.NO_CONTENT, StringUtils.EMPTY);
    }

    public static <T> ResponseBody<T> error(HttpStatus status, String message) {
        return of(status, null, message);
    }

    public static <T> ResponseBody<T> error(ResponseBody<?> responseBody) {
        return error(responseBody.getStatus(), responseBody.getMessage());
    }

    public static <T> ResponseBody<T> error(int status, String message) {
        return of(HttpStatus.valueOf(status), null, message);
    }

    public static <T> ResponseBody<T> error(HttpStatus status, MessageHandler message) {
        return of(status, null, Optional.ofNullable(message).map(MessageHandler::getMessage).orElse(StringUtils.EMPTY));
    }

    public static <T> ResponseBody<T> of(HttpStatus status, T body, String message) {
        return new ResponseBody<>(status, body, message);
    }

    public static <T> boolean hasAnyError(ResponseBody<?>... bodies) {
        if (bodies == null)
            return false;
        for (ResponseBody<?> body : bodies) {
            if (body.hasError())
                return true;
        }
        return false;
    }

}
