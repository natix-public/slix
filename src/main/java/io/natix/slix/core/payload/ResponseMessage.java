package io.natix.slix.core.payload;

public class ResponseMessage implements Payload {
    private Integer statusCode;

    private String message;

    public Integer getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(Integer statusCode) {
        this.statusCode = statusCode;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public ResponseMessage() {
    }

    public ResponseMessage(Integer statusCode, String message) {
        this.statusCode = statusCode;
        this.message = message;
    }

    public static ResponseMessage of(Integer statusCode, String message) {
        return new ResponseMessage(statusCode, message);
    }

    public static ResponseMessage of(String message) {
        return new ResponseMessage(200, message);
    }

    @Override
    public String toString() {
        return "RegistrationResponse{" +
                "statusCode=" + statusCode +
                ", message='" + message + '\'' +
                '}';
    }
}
