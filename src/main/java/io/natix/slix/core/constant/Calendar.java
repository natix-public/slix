package io.natix.slix.core.constant;

public enum Calendar {
    JALALI,
    MILADI,
    HIJRI
}
