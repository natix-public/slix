package io.natix.slix.core.constant;

public enum AgentStatus {
    NONE,
    PENDING,
    CONFIRMED,
    REJECTED
}
