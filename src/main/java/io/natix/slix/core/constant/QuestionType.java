package io.natix.slix.core.constant;

public enum QuestionType {
    Relation(0),
    Position(1),
    Certificate(2),
    Permission(3),
    SHARING(4),
    STORAGE(5);

    private final Integer value;

    public Integer getValue() {
        return value;
    }

    QuestionType(Integer value) {
        this.value = value;
    }

    public static QuestionType of(Integer value) {
        if (Relation.getValue().equals(value))
            return Relation;
        if (Position.getValue().equals(value))
            return Position;
        if (Certificate.getValue().equals(value))
            return Certificate;
        if (Permission.getValue().equals(value))
            return Permission;
        if (SHARING.getValue().equals(value))
            return SHARING;
        if (SHARING.getValue().equals(value))
            return SHARING;
        return null;
    }

    public static QuestionType of(String value) {
        return of(Integer.valueOf(value));
    }
}
