package io.natix.slix.core.constant;

public enum ConfirmStatus {
    Reject(0),
    Confirm(1),
    Modify(2),
    OnHold(3);

    private final Integer value;

    public Integer getValue() {
        return value;
    }

    ConfirmStatus(Integer value) {
        this.value = value;
    }

    public static ConfirmStatus of(Integer value) {
        if (Reject.getValue().equals(value))
            return Reject;
        if (Confirm.getValue().equals(value))
            return Confirm;
        if (Modify.getValue().equals(value))
            return Modify;
        if (OnHold.getValue().equals(value))
            return OnHold;
        return null;
    }

    public static ConfirmStatus of(String value) {
        return of(Integer.valueOf(value));
    }
}
