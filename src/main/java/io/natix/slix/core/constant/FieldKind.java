package io.natix.slix.core.constant;

public enum FieldKind {
    TEXT,
    BOOL,
    NUMERIC,
    EMAIL,
    IBAN,
    MOBILE,
    PHONE,
    ZIP_CODE,
    DATE,
    DATE_TIME,
    TIME,
    MEDIA,
    POINT
}
