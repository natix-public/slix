package io.natix.slix.core.exception;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import io.natix.slix.core.constant.HttpStatus;
import io.natix.slix.core.message.MessageHandler;
import okhttp3.Protocol;
import okhttp3.internal.http.RealResponseBody;
import org.apache.commons.lang3.ObjectUtils;
import org.apache.commons.lang3.StringUtils;
import retrofit2.Response;

import java.io.Serializable;
import java.util.Map;
import java.util.Optional;

public class HttpException extends retrofit2.HttpException {

    private final HttpErrorMessage errorMessage;

    public HttpException(MessageHandler message, HttpStatus status) {
        this(new HttpErrorMessage(Optional.ofNullable(status).orElse(HttpStatus.INTERNAL_SERVER_ERROR), message.getMessage(), message.getCode()));
    }

    public HttpException(String message, HttpStatus status) {
        this(new HttpErrorMessage(Optional.ofNullable(status).orElse(HttpStatus.INTERNAL_SERVER_ERROR), message, null));
    }

    public HttpException(HttpErrorMessage error) {
        super(
                Response.error(
                        new RealResponseBody("application/json",
                                Optional.ofNullable(error).map(HttpErrorMessage::getMessage).orElse(StringUtils.EMPTY).length(),
                                null),
                        new okhttp3.Response.Builder()
                                .protocol(Protocol.HTTP_1_1)
                                .request((new okhttp3.Request.Builder()).url("http://localhost/").build())
                                .code(Optional.ofNullable(error).map(HttpErrorMessage::getHttpStatus).orElse(HttpStatus.INTERNAL_SERVER_ERROR).value())
                                .message(Optional.ofNullable(error).map(HttpErrorMessage::getMessage).orElse(StringUtils.EMPTY))
                                .build()));
        this.errorMessage = error;
    }

    public void setPath(String path) {
        if (errorMessage != null)
            errorMessage.setPath(path);
    }

    public HttpErrorMessage getErrorMessage() {
        return errorMessage;
    }

    public HttpStatus getHttpStatus() {
        return ObjectUtils.isNotEmpty(errorMessage) && ObjectUtils.isNotEmpty(errorMessage.getHttpStatus()) ? errorMessage.getHttpStatus() : HttpStatus.NOT_IMPLEMENTED;
    }

    public Map<String, Serializable> body() {
        return errorMessage.body();
    }

    @Override
    public String toString() {
        return toString(errorMessage);
    }

    protected String toString(Object value) {
        try {
            ObjectMapper Obj = new ObjectMapper();
            return Obj.writeValueAsString(value);
        } catch (JsonProcessingException e) {
            return "";
        }
    }
}
