package io.natix.slix.core.event;

import io.natix.slix.core.payload.Event;

public interface EventHandler {

    String execute(Event event) throws Throwable;

}
