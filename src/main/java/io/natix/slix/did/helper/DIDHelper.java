package io.natix.slix.did.helper;

import io.natix.slix.core.constant.HttpStatus;
import io.natix.slix.core.helper.LogHelper;
import io.natix.slix.core.http.HttpHelper;
import io.natix.slix.core.payload.ResponseBody;
import io.natix.slix.did.constant.*;
import io.natix.slix.did.payload.DIDDocument;
import io.natix.slix.did.payload.DIDDocumentRequest;
import org.apache.commons.lang3.StringUtils;

import java.util.HashSet;
import java.util.Optional;
import java.util.Set;

public interface DIDHelper {

    default ResponseBody<DIDDocument> add(DIDDocumentRequest request, UserType kind) {
        try {
            return ResponseBody.ok(HttpHelper.call(DIDConstant.getDIDService().add(kind, request)));
        } catch (Exception e) {
            LogHelper.error("DIDException", e.getMessage());
            return ResponseBody.error(HttpStatus.SERVICE_UNAVAILABLE, Messages.DID_SERVICE_IS_UNAVAILABLE);
        }
    }

    default ResponseBody<DIDDocument> findById(String id) {
        try {
            if (StringUtils.isEmpty(id))
                return ResponseBody.error(HttpStatus.BAD_REQUEST, Messages.ID_IS_REQUIRED);
            return ResponseBody.ok(HttpHelper.call(DIDConstant.getDIDService().read(id)));
        } catch (Exception e) {
            LogHelper.error("DIDException", e.getMessage());
            return ResponseBody.error(HttpStatus.SERVICE_UNAVAILABLE, Messages.DID_SERVICE_IS_UNAVAILABLE);
        }
    }

    default ResponseBody<DIDDocument> findByIdAndType(String id, UserType type) {
        return findById(id)
                .filter(x -> type.name().equalsIgnoreCase(x.getType()))
                .or(() -> ResponseBody.error(HttpStatus.NOT_FOUND, Messages.DID_NOT_FOUND));
    }

    default ResponseBody<String> findPublicKey(String id, UserType userType, EncryptionKind encryptionKind) {
        return findByIdAndType(id, userType)
                .map(x -> findPublicKey(x, encryptionKind))
                .orElse(ResponseBody.error(HttpStatus.NOT_FOUND, Messages.DID_NOT_FOUND));
    }

    default ResponseBody<String> findPublicKey(DIDDocument document, EncryptionKind encryptionKind) {
        return Optional.ofNullable(document)
                .map(x -> Set.of(x.getPublicKey()))
                .orElse(new HashSet<>())
                .stream().filter(x -> encryptionKind.name().equalsIgnoreCase(x.getType()))
                .findFirst()
                .map(x -> ResponseBody.ok(x.getPublicKey()))
                .orElse(ResponseBody.error(HttpStatus.NOT_FOUND, Messages.DID_NOT_FOUND));
    }

    default ResponseBody<String> findServiceEndpoint(String id, UserType userType, ServiceKind serviceKind) {
        return findByIdAndType(id, userType)
                .map(x -> findServiceEndpoint(x, serviceKind))
                .orElse(ResponseBody.error(HttpStatus.NOT_FOUND, Messages.DID_NOT_FOUND));
    }

    default ResponseBody<String> findServiceEndpoint(DIDDocument document, ServiceKind serviceKind) {
        return Optional.ofNullable(document)
                .map(x -> Set.of(x.getService()))
                .orElse(new HashSet<>())
                .stream().filter(x -> serviceKind.name().equalsIgnoreCase(x.getType()))
                .findFirst()
                .map(x -> ResponseBody.ok(x.getServiceEndpoint()))
                .orElse(ResponseBody.error(HttpStatus.NOT_FOUND, Messages.DID_NOT_FOUND));
    }
}
