package io.natix.slix.did.infrastructure;

import io.natix.slix.did.constant.UserType;
import io.natix.slix.did.payload.DIDDocument;
import io.natix.slix.did.payload.DIDDocumentRequest;
import retrofit2.Call;
import retrofit2.http.*;

public interface DIDService {

    @POST("/users/{kind}")
    Call<DIDDocument> add(@Path("kind") UserType kind, @Body DIDDocumentRequest request);

    @GET("/users/{id}")
    Call<DIDDocument> read(@Path("id") String id);
}
