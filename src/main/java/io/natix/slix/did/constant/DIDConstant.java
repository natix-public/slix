package io.natix.slix.did.constant;

import io.natix.slix.core.http.HttpHelper;
import io.natix.slix.core.util.EnvironmentUtil;
import io.natix.slix.did.infrastructure.DIDService;

public class DIDConstant {

    static String getDIDUrl() {
        return EnvironmentUtil.getPropertyValue("slix.did.url", "http://127.0.0.1:3000");
    }

    public static DIDService getDIDService() {
        return HttpHelper.getHttpInstance(getDIDUrl(), DIDService.class);
    }

}
