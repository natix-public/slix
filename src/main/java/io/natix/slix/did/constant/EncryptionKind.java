package io.natix.slix.did.constant;

public enum  EncryptionKind {
    EDDSA,
    ECDH
}
