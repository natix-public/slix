package io.natix.slix.did.constant;

public enum UserType {
    PROVER,
    SUBJECT,
    REQUESTER
}
