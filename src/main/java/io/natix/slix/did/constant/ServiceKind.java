package io.natix.slix.did.constant;

public enum ServiceKind {
    SUBSCRIBE,
    REPLY
}
