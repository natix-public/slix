package io.natix.slix.did.constant;

import io.natix.slix.core.message.MessageHandler;

public enum Messages implements MessageHandler {
    DID_NOT_FOUND,
    SERVICE_UNAVAILABLE,
    ID_IS_REQUIRED,
    DID_SERVICE_IS_UNAVAILABLE
}
